# This is a repository for examples N8N - workflow automation using IFTTT

## Quick start
You can run N8N in Docker

```bash
docker run -it --rm --name n8n -p 5678:5678 -v n8n:/home/node/.n8n n8nio/n8n
```

and then open URL http://localhost:5678/ in your browser to import json from a URL or file.


